.stm8

Flash_Start    EQU  0x8000; 

PD_ODR    EQU  0x500F;
PD_IDR    EQU  0x5010;
PD_DDR    EQU  0x5011;
PD_CR1    EQU  0x5012;
PD_CR2    EQU  0x5013;

;RAM
MEM0      EQU  0x0;
MEM1      EQU  0x1;
MEM2      EQU  0x2;
MEM3      EQU  0x3;

.org Flash_Start
BSET PD_DDR,#$1
BSET PD_CR1,#$1
BSET PD_DDR,#$2
BSET PD_CR1,#$2
BSET PD_DDR,#$4
BSET PD_CR1,#$4
BSET PD_DDR,#$5
BSET PD_CR1,#$5
BSET PD_DDR,#$6
BSET PD_CR1,#$6

JP MAIN

H:
     BRES PD_ODR,#$1
     BSET PD_ODR,#$2
     BRES PD_ODR,#$3
     BSET PD_ODR,#$4
     BRES PD_ODR,#$5
     BRES PD_ODR,#$6
     BRES PD_ODR,#$7
     RET;

O:
     BRES PD_ODR,#$1
     BRES PD_ODR,#$2
     BRES PD_ODR,#$3
     BRES PD_ODR,#$4
     BRES PD_ODR,#$5
     BSET PD_ODR,#$6
     BRES PD_ODR,#$7
     RET;

L:
     BSET PD_ODR,#$1
     BRES PD_ODR,#$2
     BRES PD_ODR,#$3
     BSET PD_ODR,#$4
     BSET PD_ODR,#$5
     BSET PD_ODR,#$6
     BRES PD_ODR,#$7
     RET;

A:
     BRES PD_ODR,#$1
     BSET PD_ODR,#$2
     BRES PD_ODR,#$3
     BRES PD_ODR,#$4
     BRES PD_ODR,#$5
     BRES PD_ODR,#$6
     BRES PD_ODR,#$7
     RET;

SLEEP:
     MOV MEM0,$FFFFFFFFFFFFFFFF;
     MOV MEM1,$FFFFFFFFFFFFFFFF;
     MOV MEM2,$F;
     sleeploop:
          sleep_aux1:
          sleep_aux2:
               DEC MEM2;
               JRNE sleep_aux2;
          MOV MEM2,$F
          DEC MEM1;
          JRNE sleep_aux1;
     MOV MEM1,$FFFFFFFFFFFFFFFF;
     DEC MEM0;
     JRNE sleeploop;
     RET;

MAIN:
     CALL H
     CALL SLEEP
     CALL O
     CALL SLEEP
     CALL L
     CALL SLEEP
     CALL A
     CALL SLEEP
     JP MAIN
